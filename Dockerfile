FROM moodlehq/moodle-php-apache:8.1

RUN git clone -b MOODLE_405_STABLE --depth 1 git://git.moodle.org/moodle.git /var/www/html

COPY --from=ghcr.io/phpstan/phpstan:1-php8.1 /composer/vendor/phpstan/phpstan/phpstan.phar /var/www/html/phpstan.phar

COPY phpstan.neon /var/www/html/phpstan.neon

COPY stubs /var/www/html/stubs

COPY phpstanconstants.php /var/www/html/phpstanconstants.php
