# Moodlephpstan

## Build


```
docker build -t registry.gitlab.com/eabc-it/publicdocker/moodlephpstan:4.5 .
```

## Modo de uso

```
docker run --rm -v [/path/to/plugin]:/var/www/html/[path/to/plugin] registry.gitlab.com/eabc-it/publicdocker/moodlephpstan:4.5 php /var/www/html/phpstan.phar analyze [path/to/plugin] --level 5 --memory-limit 3000M
```
